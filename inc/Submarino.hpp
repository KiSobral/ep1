#ifndef SUBMARINO_HPP
#define SUBMARINO_HPP

#include "Embarcacoes.hpp"

class Submarino : public Embarcacoes {
    public:
        Submarino();
        Submarino(int linha, int coluna, string direcionamento);
        ~Submarino();

        int habilidade_especial(int vida);
};


#endif