#ifndef PLAYER_HPP
#define PLAYER_HPP

#include <string>
#include <vector>
#include <fstream>

#include "Embarcacoes.hpp"
#include "Canoa.hpp"
#include "Submarino.hpp"
#include "PortaAvioes.hpp"

using namespace std;

class Player{
    private:
        string nome;
        int MatrizPlayer[16][16];
        int MatrizInimiga[16][16];
        int magnitude;

        vector <Embarcacoes *> TotaldeBarcos;

        fstream le_frotas;

        Player();

        string LinhasDoArquivo;

        // Variáveis usadas na seleção das frotas
        int NumFrotas;                      // Responsável por mostrar quantas frotas estão disponíveis
        string ComparaNumeros;              // String que será usada na comparação do header da frota
        string ComparaIntermediario;        // Comparador intermediário
        bool FlagFrota;                     // Flag para a condição da criação da frota
        int ContaBarcos;                    // Delimitador de embarcações

        // Variáveis para o split
        vector <string> ArmazenaSplit;      // Vector que recebe as palavras separadas da frase 
        char CharIntermediario;             // Char no qual a String intermediária dará push_back
        string StringIntermediaria;         // String na qual a ArmazenaSplit dará push_back
        size_t i;                           // Iterador
        int AtualizaFrotas;                 // Variável usada para atualizar o valor das frotas

    public:
        Player (string nome, int magnitude);
        ~Player();

        string get_nome();
        void set_nome(string nome);

        int get_NumFrotas();

        int get_MatrizPlayer(int linha, int coluna);
        int get_MatrizInimiga(int linha, int coluna);
        void set_MatrizInimiga(int linha, int coluna, int valor);

        void informa_frotas();
        void escolhe_frotas(int Frota);
        void guarda_frotas(int Frota);

        void imprime_matriz();
        void imprime_MatrizInimiga();

        bool verifica_vitoria();        

        void Split(string frase);



};

#endif 