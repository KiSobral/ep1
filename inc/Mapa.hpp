#ifndef MAPA_HPP
#define MAPA_HPP

#include <string>
#include <fstream>
#include <vector>

#include "Embarcacoes.hpp"
#include "PortaAvioes.hpp"
#include "Submarino.hpp"
#include "Canoa.hpp"

using namespace std;

class Mapa {
    private:
        int magnitude;
        int matriz_mapa[16][16];
        fstream acervo_de_mapas;
        vector <Embarcacoes*> FrotaDeBarcos;
        
        string leitura_arquivo;
        
        // Variáveis para o split
        vector <string> ArmazenaSplit;      // Vector que recebe as palavras separadas da frase 
        char CharIntermediario;             // Char no qual a String intermediária dará push_back
        string StringIntermediaria;         // String na qual a ArmazenaSplit dará push_back
        size_t i;                           // Iterador
        int AtualizaFrotas;                 // Variável usada para atualizar o valor das frotas

        Mapa();

    public:
        Mapa (int magnitude);
        ~Mapa();

        int get_magnitude();
        void set_magnitude(int magnitude);

        int get_ContaPalavras();

        // Função que adiciona embarcações dentro do vector
        void adiciona_frota(vector<Embarcacoes*> FrotaDeBarcos);

        // Função responsável por guardar os dados do mapa em um arquivo
        void armazena_mapa();

        // Implementação do split
        void Split(string frase);

};

#endif