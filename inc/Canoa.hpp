#ifndef CANOA_HPP
#define CANOA_HPP

#include "Embarcacoes.hpp"

class Canoa : public Embarcacoes {
    public:
        Canoa();
        Canoa(int linha, int coluna); 
        ~Canoa();

        int get_vida();
        void set_vida(int vida);

        int habilidade_especial(int vida);
};


#endif