#ifndef EMBARCACOES_HPP
#define EMBARCACOES_HPP

#include <string>

using namespace std;

// Superclasse para instanciar as embarcações usadas no jogo
class Embarcacoes {
    protected:
        string nome;
        int tamanho;        
        int linha;
        int coluna;
        string direcionamento;

    public:
        Embarcacoes();
        ~Embarcacoes();

        string get_nome();
        void set_nome(string nome);

        int get_tamanho();
        void set_tamanho(int tamanho);

        int get_linha();
        void set_linha(int linha);
        
        int get_coluna();
        void set_coluna(int coluna);

        string get_direcionamento();
        void set_direcionamento(string direcionamento);
        
        virtual int habilidade_especial(int vida)=0;
};


#endif