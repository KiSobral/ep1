#ifndef PORTAAVIOES_HPP
#define PORTAAVIOES_HPP

#include "Embarcacoes.hpp"

class PortaAvioes : public Embarcacoes {
    public:
        PortaAvioes();
        PortaAvioes(int linha, int coluna, string direcionamento);
        ~PortaAvioes();

        int habilidade_especial(int vida);
};



#endif