#ifndef MENUS_HPP
#define MENUS_HPP

#include <string>
#include <vector>

#include "Player.hpp"
#include "Mapa.hpp"
#include "Embarcacoes.hpp"
#include "PortaAvioes.hpp"
#include "Submarino.hpp"
#include "Canoa.hpp"

using namespace std;

class Menus{
    private:
        // Variável usada ao longo de todo o código
        string ControleDeOpcao;

        // Variáveis usadas em Multiplayer
        Player * JogadorN1;
        Player * JogadorN2;
        string NomeDosPlayers;
        int FrotaPlayer;
        Canoa HabilidadeCanoa;
        Submarino HabilidadeSubmarino;
        PortaAvioes HabilidadePortaAvioes;

        // Flags para o main loop
        bool MainLoop;  // Enquanto verdadeiro o 
        bool Turno;     // True = JOGADOR N1 || False = JOGADOR N2
        int MenorNum;

        // Variável usada em Multiplayer e CriarFrota
        int magnitude;
        int LinhaBarco;
        int ColunaBarco;

        // Variáveis usadas em 'CriarFrota'
        int NumBarcos;

        Mapa * GuardaFrota;

        int Matriz[16][16];
        string DirecionamentoBarco;
        vector <Embarcacoes*> FrotaBarco;

    public:
        Menus();
        ~Menus();
        
        // Implementação do split
        //void Split(string LeituraArquivo);       
        
        // Voids para a estética do jogo
        void ImprimeTitulo();
        void ImprimeMenuOpcoes();

        // Voids de direcionamento de opção
        void Multiplayer();
        void EscolhaOpcao();
        void ContinuarJogo();
        void RetornaAoMenu();


        // Menus do jogo
        void CriarFrota();
        void CriarMapa();
        void Sobre_O_Jogo();
        int SaidaDoJogo();
        int MenuPrincipal();
};

#endif
