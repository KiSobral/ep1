># EP1 - OO 2019.1 
	>
	>## Apresentação
		>Este é o resultado final da ep1 2019/1.
		>O projeto trata-se da implementação do jogo de tabuleiro ***Batalha Naval*** como um jogo de terminal.
		>
		>Toda a documentação *.txt* está localizada na pasta *doc*.
		> 
		>O README do projeto está separado em:
			>- **Como rodar o jogo**
			>- **Funcionaidades**
			>- **Multiplayer**
			>- **Criar frota**
			>- **Sobre o jogo**
			>- **Sair do jogo**
			>- **Herança**
			>- **Polimorfismo**
			>- **Agregação e composição**
			>- **Classes**
	>	
	>	
	>## Como rodar o jogo
		>O jogo está sendo compilado utilizando a ferramenta MakeFile disponibilizada originalmente no repositório da ep1.
		>Para o funcionamento do jogo, foram usadas as bibliotecas:
			>- **iostream**
			>- **string**
			>- **fstream**
			>- **vector**
			>- **stdlib.h** 
		>
		>Para compilar os arquivos *.cpp* e *.hpp* em arquivos *.o*, basta digitar o comando *make* no terminal dentro da pasta *ep1*.
		>Em seguida, para executar os arquivos *.o* basta digitar o comando *make run*.
		>Caso seja necessário, para remover os arquivos *.o* basta digitar o comando *make clean*.
	>		
	>	
	>## Funcionalidades
		>Dentro do projeto, estão disponíveis 4 funcionalidades para o usuário.
		>Estas são:
			>- **Multiplayer**
			>- **Criar Frota**
			>- **Sobre o Jogo**
			>- **Sair do Jogo**
		>
		>
	>## Multiplayer
		>Esta opção está dividida em dois grandes estágios:
			>- **Instanciar os players**
			>- **Main loop**
		>
		>### Instanciar os players
			>Esta parte consiste em, a priori, escolher o tamanho do mapa.
			>Após ter o tamanho definido, os players deverão inserir seus *"nicknames"*, escolher uma frota pré-definida pela função ***Criar Frota***
			>e confirmar sua escolha.
		>	
		>### Main loop
			>O main loop tem como condição de parada a destruição total de uma das duas frotas em jogo.
			>A cada iteração um player bombardeia uma posição no mapa inimigo.
		>	
		>
	>## Criar Frota
		>Para esta função, o usuário deve escolher a magnitude do mapa, em seguida, definir a posição de 8 canoas,
		>definir a posição e o direcionamento de 4 submarinos e, por fim, definir a posição e o direcionamento de 2 porta-aviões.
	>		
	>		
	>## Sobre o jogo
		>Esta função basicamente fala um pouco sobre o backstage do código.
		>
		>
	>## Sair do jogo
		>Esta função é usada para encerrar a execução do jogo.
		>
		>
	>## Herança
		>O uso de Herança, dentro dos conceitos de Programação Orientada a Objetos, esta justamente nas classes de Embarcações.
		>Tanto a classe *Canoa*, quanto *Submarino* e *PortaAvioes* herdam de *Embarcações*.
		>
		>
	>## Polimorfismo
		>### Sobrecarga
			>A **sobrecarga** dentro do código ocorre com frequência nos construtores das Embarcações.
			>	
		>### Sobrescrita
			>A **sobrescrita** dentro do código occore com frequência na função *habilidade_especial* das embarcações.
			>
			>
	>## Agregação e Composição
		>Dentro do código, objetos de classes distintas foram instanciados dentro de uma classe.
		>A classe onde tal fenômeno ocorre com mais frequência é a classe *Menus*.
		>
		>
	>## Classes 
		>As classes do jogo são:
			>- **Embarcacoes**
			>- **Canoa** *(herda de Embarcacoes)*
			>- **Submarino** *(herda de Embarcacoes)*
			>- **PortaAvioes** *(herda de Embarcacoes)*
			>- **Mapa**
			>- **Player**
			>- **Menus**