#include <iostream>
#include <string>
#include <fstream>
#include <vector>

#include "Mapa.hpp"
#include "Embarcacoes.hpp"
#include "PortaAvioes.hpp"
#include "Submarino.hpp"
#include "Canoa.hpp"

using namespace std;

// Contrutor da classe que está definido como privado a fim de proibir o uso deste 
Mapa::Mapa(){}

// Construtor sobrecarregado
Mapa::Mapa(int magnitude){
    for (int i=0; i<16; i++){
        for (int j=0; j<16; j++){
            matriz_mapa[i][j]=0;
        }
    }
    
    if (magnitude==13) acervo_de_mapas.open("./doc/magnitude_13.txt");
    else if (magnitude==14) acervo_de_mapas.open("./doc/magnitude_14.txt");
    else if (magnitude==15) acervo_de_mapas.open("./doc/magnitude_15.txt");

}

// Destrutor da classe. Este é usado para fechar o arquivo.
Mapa::~Mapa(){
    acervo_de_mapas.close();
    FrotaDeBarcos.clear();
}
 
// Getter e Setter da magnitude
int Mapa::get_magnitude(){
    return magnitude;
}
void Mapa::set_magnitude(int magnitude){
    this->magnitude = magnitude;
}

// Função para a frota de barcos
void Mapa::adiciona_frota(vector<Embarcacoes*> FrotaDeBarcos){
    this->FrotaDeBarcos = FrotaDeBarcos;
}

// Função usada para escrever os dados do mapa no arquivo
void Mapa::armazena_mapa(){
    // Leitura de quantas frotas estão disponíveis e atualização de frotas
    acervo_de_mapas.seekp(0, acervo_de_mapas.beg);
    getline(acervo_de_mapas, leitura_arquivo);
    Mapa::Split(leitura_arquivo);
    AtualizaFrotas = stol (ArmazenaSplit[0]);
    AtualizaFrotas++;

    // Atualizando quantas frotas estão disponíveis
    acervo_de_mapas.seekp(0, acervo_de_mapas.beg);
    acervo_de_mapas << AtualizaFrotas << " " << ArmazenaSplit[1] << " " << ArmazenaSplit[2] << endl << endl;

    // Escrevendo a nova frota no arquivo 
    acervo_de_mapas.seekp(0, acervo_de_mapas.end);
    acervo_de_mapas << "#" << AtualizaFrotas << endl;
    for (int i=0; i<14; i++){
        acervo_de_mapas << FrotaDeBarcos[i]->get_linha() << " " << FrotaDeBarcos[i]->get_coluna() << " " << FrotaDeBarcos[i]->get_nome() << " " << FrotaDeBarcos[i]->get_direcionamento() << endl;

        if (i==7 || i==11 || i==13) acervo_de_mapas << endl;
    } 
}

// Implementação da função split
void Mapa::Split(string frase){
    ArmazenaSplit.clear();
    StringIntermediaria.clear();

    for (i=0; i<frase.size(); i++){
        CharIntermediario = frase[i];

        if (CharIntermediario != ' '){
            StringIntermediaria.push_back(CharIntermediario);
        } 

        if (CharIntermediario == ' ' || i==(frase.size()-1) ){
            ArmazenaSplit.push_back(StringIntermediaria);
            StringIntermediaria.clear();
        }
    }
}