#include "Canoa.hpp"
#include <iostream>

using namespace std;

Canoa::Canoa(){
    set_nome("canoa");
    set_tamanho(1);
    set_linha(-1);
    set_coluna(-1);
    set_direcionamento("nada");
}

Canoa::Canoa(int linha, int coluna){
    set_nome("canoa");
    set_tamanho(1);
    set_linha(linha);
    set_coluna(coluna);
    set_direcionamento("nada");
}

Canoa::~Canoa(){}


int Canoa::habilidade_especial(int vida){
    cout << "Você atingiu uma canoa";
    return vida;
}