#include <iostream>
#include <string>
#include <fstream>
#include <stdlib.h>

#include "Menus.hpp"
#include "Embarcacoes.hpp"
#include "PortaAvioes.hpp"
#include "Submarino.hpp"
#include "Canoa.hpp"

using namespace std;

int main(){
    Menus * MenuDoJogo = new Menus;
    MenuDoJogo->MenuPrincipal();
    delete MenuDoJogo;
}
