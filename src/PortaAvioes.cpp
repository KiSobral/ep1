#include "PortaAvioes.hpp"
#include <iostream>

using namespace std;

PortaAvioes::PortaAvioes(){
    set_nome("porta-avioes");
    set_tamanho(4);
    set_linha(-1);
    set_coluna(-1);
    set_direcionamento("nada");
}

PortaAvioes::PortaAvioes(int linha, int coluna, string direcionamento){
    set_nome("porta-avioes");
    set_tamanho(4);
    set_linha(linha);
    set_coluna(coluna);
    set_direcionamento(direcionamento);
}

PortaAvioes::~PortaAvioes(){}

int PortaAvioes::habilidade_especial(int vida){
    int retorno;
    for (int i=0; i<vida; i++) retorno = rand() %3;

    if (retorno==2){
        cout << "O porta-aviões conseguiu desviar seu míssil!" << endl;
    } else {
        cout << "O porta-aviões não conseguiu desviar o ataque e foi atingido!" << endl;
    }

    return retorno;
}