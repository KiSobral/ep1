#include "Embarcacoes.hpp"
#include <iostream>

using namespace std;

Embarcacoes::Embarcacoes(){
    set_nome("");
    set_tamanho(-1);
    set_linha(-1);
    set_coluna(-1);
    set_direcionamento("");
}

Embarcacoes::~Embarcacoes(){}

// Getter e Setter do nome
string Embarcacoes::get_nome(){
    return nome;
}
void Embarcacoes::set_nome(string nome){
    this->nome = nome;
}

// Getter e Setter do tamanho
int Embarcacoes::get_tamanho(){
    return tamanho;
}
void Embarcacoes::set_tamanho(int tamanho){
    this->tamanho = tamanho;
}

// Getter e Setter da linha
int Embarcacoes::get_linha(){
    return linha;
}
void Embarcacoes::set_linha(int linha){
    this->linha = linha;
}

// Getter e Setter da coluna
int Embarcacoes::get_coluna(){
    return coluna;
}
void Embarcacoes::set_coluna(int coluna){
    this->coluna = coluna;
}

// Getter e Setter do direcionamento
string Embarcacoes::get_direcionamento(){
    return direcionamento;
}
void Embarcacoes::set_direcionamento(string direcionamento){
    this->direcionamento = direcionamento;
}

