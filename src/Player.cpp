#include "Player.hpp"

#include <iostream>
#include <vector>

#include "Embarcacoes.hpp"
#include "Canoa.hpp"
#include "Submarino.hpp"
#include "PortaAvioes.hpp"

using namespace std;

Player::Player(){
    set_nome("");
    for (int i=0; i<16; i++){
        for (int j=0; j<16; j++){
            MatrizPlayer[i][j]=0;
        }
    }
}

Player::Player(string nome, int magnitude){
    set_nome(nome);
    this->magnitude = magnitude;
    for (int i=0; i<16; i++){
        for (int j=0; j<16; j++){
            MatrizPlayer[i][j]=0;
        }
    }
    if (magnitude==13) le_frotas.open("./doc/magnitude_13.txt");
    else if (magnitude==14) le_frotas.open("./doc/magnitude_14.txt");
    else if (magnitude==15) le_frotas.open("./doc/magnitude_15.txt");
}

Player::~Player(){
    le_frotas.close();
}

// Setter e getter do nome
string Player::get_nome(){
    return nome;
}
void Player::set_nome(string nome){
    this->nome = nome;
}

// Getter do número de frotas
int Player::get_NumFrotas(){
    return NumFrotas;
}

// Setters e getters das matrizes
int Player::get_MatrizPlayer(int linha, int coluna){
    return MatrizPlayer[linha][coluna];
}
int Player::get_MatrizInimiga(int linha, int coluna){
    return MatrizInimiga[linha][coluna];
}
void Player::set_MatrizInimiga(int linha, int coluna, int valor){
    this->MatrizInimiga[linha][coluna] = valor;
}

// Função responsável por mostrar as frotas disponíveis
void Player::informa_frotas(){
    le_frotas.seekp(0, le_frotas.beg);
    NumFrotas=0;

    getline(le_frotas, LinhasDoArquivo);
    Player::Split(LinhasDoArquivo);
 
    NumFrotas = stol (ArmazenaSplit[0]);    

    cout << "Existem " << NumFrotas << " disponíveis para esta magnitude." << endl;
    cout << "Para visualizar a frota, insira um número de 1 a " << NumFrotas << "." << endl;

}

// Função responsável por escolher a frota e montar a matriz do player
void Player::escolhe_frotas(int Frota){
    for (int i=0; i<16; i++){
        for (int j=0; j<16; j++){
            MatrizPlayer[i][j]=0;
        }
    }

    ComparaNumeros.clear();
    ComparaIntermediario = to_string(Frota);
    ComparaNumeros.push_back('#');
    ComparaNumeros.append(ComparaIntermediario);
    
    FlagFrota=false;
    ContaBarcos=0;

    le_frotas.seekg(0, le_frotas.beg);
    while (getline(le_frotas, LinhasDoArquivo)){
        if (FlagFrota==true){
            if (ContaBarcos>0 &&  ContaBarcos<9){
                Player::Split(LinhasDoArquivo);
                MatrizPlayer[ stol(ArmazenaSplit[0]) ][ stol(ArmazenaSplit[1]) ] = 1;
            }

            if (ContaBarcos>9 && ContaBarcos<14){
                Player::Split(LinhasDoArquivo);
                if (!ArmazenaSplit[3].compare("cima")){
                    for (int i=0; i<2; i++) MatrizPlayer[ stol(ArmazenaSplit[0]) - i][ stol(ArmazenaSplit[1]) ] = 2;
                } else if (!ArmazenaSplit[3].compare("baixo")){
                    for (int i=0; i<2; i++) MatrizPlayer[stol(ArmazenaSplit[0]) + i][ stol(ArmazenaSplit[1]) ] = 2;
                } else if (!ArmazenaSplit[3].compare("direita")){
                    for (int i=0; i<2; i++) MatrizPlayer[ stol(ArmazenaSplit[0]) ][ stol(ArmazenaSplit[1]) + i] = 2;
                } else if (!ArmazenaSplit[3].compare("esquerda")){
                    for (int i=0; i<2; i++) MatrizPlayer[ stol(ArmazenaSplit[0]) ][ stol(ArmazenaSplit[1]) - i] = 2;
                }
            }

            if (ContaBarcos>14 && ContaBarcos<17){
                Player::Split(LinhasDoArquivo);
                if (!ArmazenaSplit[3].compare("cima")){
                    for (int i=0; i<4; i++) MatrizPlayer[ stol(ArmazenaSplit[0]) - i][ stol(ArmazenaSplit[1]) ] = 3;
                } else if (!ArmazenaSplit[3].compare("baixo")){
                    for (int i=0; i<4; i++) MatrizPlayer[stol(ArmazenaSplit[0]) + i][ stol(ArmazenaSplit[1]) ] = 3;
                } else if (!ArmazenaSplit[3].compare("direita")){
                    for (int i=0; i<4; i++) MatrizPlayer[ stol(ArmazenaSplit[0]) ][ stol(ArmazenaSplit[1]) + i] = 3;
                } else if (!ArmazenaSplit[3].compare("esquerda")){
                    for (int i=0; i<4; i++) MatrizPlayer[ stol(ArmazenaSplit[0]) ][ stol(ArmazenaSplit[1]) - i] = 3;
                }
            }

            ContaBarcos++;
            if (ContaBarcos>17) FlagFrota=false;
        }

        if (!LinhasDoArquivo.compare(ComparaNumeros)){
            FlagFrota=true;
            ContaBarcos++;
        } 
    } 
}


void Player::imprime_matriz(){
    for (int i=1; i<=magnitude; i++){
        cout << "\t\t\t";
        for (int j=1; j<=magnitude; j++){
            if (MatrizPlayer[i][j]==1) cout << "C ";
            else if (MatrizPlayer[i][j]==2) cout << "S ";
            else if (MatrizPlayer[i][j]==3) cout << "P ";
            else cout << MatrizPlayer[i][j] << " ";
        }
        cout << endl;
    }
}

void Player::imprime_MatrizInimiga(){
    cout << "\t\t\t|";
    for (int k=1; k<=magnitude; k++){
        cout << "---|";
    }
    cout << endl;

    for (int i=1; i<=magnitude; i++){
        cout <<"\t\t\t|";
        for (int j=1; j<=magnitude; j++){
            if (MatrizInimiga[i][j]==0 || MatrizInimiga[i][j]==1 || MatrizInimiga[i][j]==2 || MatrizInimiga[i][j]==3){
                cout << " @ |";
            } else if (MatrizInimiga[i][j]==-1){
                cout << " X |";
            } else if (MatrizInimiga[i][j]==-2 || MatrizInimiga[i][j]==-3 || MatrizInimiga[i][j]==-4){
                cout << " O |";
            } else if (MatrizInimiga[i][j]==4){
                cout << " - |";
            }
        }
        cout << endl;

        cout << "\t\t\t|";
        for (int k=1; k<=magnitude; k++){
            cout << "---|";
        }
        cout << endl;
    }
}

bool Player::verifica_vitoria(){
    int MaiorNumero;
    for (int i=1; i<=magnitude; i++){
        for (int j=1; j<=magnitude; j++){
            if (i==1 && j==1) MaiorNumero = MatrizInimiga[i][j];
            if (MatrizInimiga[i][j]>MaiorNumero) MaiorNumero=MatrizInimiga[i][j];
        }
    }

    if (MaiorNumero<=0) return true;
    else return false;
}

// Implementação da função split
void Player::Split(string frase){
    ArmazenaSplit.clear();
    StringIntermediaria.clear();

    for (i=0; i<frase.size(); i++){
        CharIntermediario = frase[i];

        if (CharIntermediario != ' '){
            StringIntermediaria.push_back(CharIntermediario);
        } 

        if (CharIntermediario == ' ' || i==(frase.size()-1) ){
            ArmazenaSplit.push_back(StringIntermediaria);
            StringIntermediaria.clear();
        }
    }
}