#include "Submarino.hpp"
#include <iostream>

using namespace std;

Submarino::Submarino(){
    set_nome("submarino");
    set_tamanho(2);
    set_linha(-1);
    set_coluna(-1);
    set_direcionamento("nada");    
}

Submarino::Submarino(int linha, int coluna, string direcionamento){
    set_nome("submarino");
    set_tamanho(2);
    set_linha(linha);
    set_coluna(coluna);
    set_direcionamento(direcionamento);
}

Submarino::~Submarino(){}

int Submarino::habilidade_especial(int vida){
    if (vida==1){
        cout << "Seu ataque atingiu o escudo da posição!" << endl;
        cout << "Será necessário mais um ataque para afundar esta casa do Submarino." << endl;
    } else{
        cout << "Desta vez as defesas da embarcação não resistiram!" << endl;
        cout << "Você afundou esta posição." << endl;
    }

    return vida;
}