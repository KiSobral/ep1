#include <iostream>
#include <string>
#include <fstream>
#include <stdlib.h>
#include <vector>

#include "Menus.hpp"
#include "Mapa.hpp"
#include "Embarcacoes.hpp"
#include "PortaAvioes.hpp"
#include "Submarino.hpp"
#include "Canoa.hpp"

using namespace std;

Menus::Menus(){}
Menus::~Menus(){}


// Void para imprimir o título do jogo
void Menus::ImprimeTitulo(){
    system ("clear");
    cout << "__________________________________________________________________________________________________" << endl;
    cout << "|                                                                                                |" << endl;
    cout << "|      .______        ___      .___________.     ___       __       __    __       ___           |" << endl;
    cout << "|      |   _  \\      /   \\     |           |    /   \\     |  |     |  |  |  |     /   \\          |" << endl;
    cout << "|      |  |_)  |    /  ^  \\    `---|  |----`   /  ^  \\    |  |     |  |__|  |    /  ^  \\         |" << endl;
    cout << "|      |   _  <    /  /_\\  \\       |  |       /  /_\\  \\   |  |     |   __   |   /  /_\\  \\        |" << endl;
    cout << "|      |  |_)  |  /  _____  \\      |  |      /  _____  \\  |  `----.|  |  |  |  /  _____  \\       |" << endl;
    cout << "|      |______/  /__/     \\__\\     |__|     /__/     \\__\\ |_______||__|  |__| /__/     \\__\\      |" << endl;
    cout << "|                  .__   __.      ___      ____    ____      ___       __                        |" << endl;
    cout << "|                  |  \\ |  |     /   \\     \\   \\  /   /     /   \\     |  |                       |" << endl;
    cout << "|                  |   \\|  |    /  ^  \\     \\   \\/   /     /  ^  \\    |  |                       |" << endl;
    cout << "|                  |  . `  |   /  /_\\  \\     \\      /     /  /_\\  \\   |  |                       |" << endl;
    cout << "|                  |  |\\   |  /  _____  \\     \\    /     /  _____  \\  |  `----.                  |" << endl;
    cout << "|                  |__| \\__| /__/     \\__\\     \\__/     /__/     \\__\\ |_______|                  |" << endl;
    cout << "|                                                                                                |" << endl;
    cout << "|________________________________________________________________________________________________|" << endl;
}


// Void para imprimir o Menu de Opções
void Menus::ImprimeMenuOpcoes(){
    cout << "       __  __                            _            ___  " << endl;
    cout << "      |  \\/  |  ___   _ _    _  _     __| |  ___     / _ \\   _ __   __   ___   ___   ___" << endl;
    cout << "      | |\\/| | / -_) | ' \\  | || |   / _` | / -_)   | (_) | | '_ \\ / _| / _ \\ / -_) (_-<" << endl;
    cout << "      |_|  |_| \\___| |_||_|  \\_,_|   \\__,_| \\___|    \\___/  | .__/ \\__| \\___/ \\___| /__/" << endl;
    cout << "                                                            |_|" << endl ;
}


// Void para direcionar as opções do jogo
void Menus::EscolhaOpcao(){
    cout << "Escolha uma opção: ";
    cin >> ControleDeOpcao;
}


// Void para continuar o estágio do jogo
void Menus::ContinuarJogo(){
    cout << endl << "Insira qualquer tecla para continuar...";
    cin.ignore();
}


// Void para retornao ao Menu Principal
void Menus::RetornaAoMenu(){    
    cout << "Insira qualquer tecla para continuar...";
    cin.ignore();
    Menus::MenuPrincipal();
}


// Void para o main loop do jogo
void Menus::Multiplayer(){
    // Escolha do tamanho do mapa
    Menus::ImprimeTitulo();
    cout << endl << "\t\t\t\t\tTAMANHO DO MAPA" << endl;
    cout << "Existem 3 magnitudes possíveis para o mapa:" << endl;
    cout << "\t13 = Mapa com 13 linhas e 13 colunas." << endl;
    cout << "\t14 = Mapa com 14 linhas e 14 colunas." << endl;
    cout << "\t15 = Mapa com 15 linhas e 15 colunas." << endl << endl;
    cout << "Escolha a magnitude do mapa: ";
    cin >> magnitude;

    // Validação da magnitude do mapa
    if (magnitude<13){ 
        Menus::ImprimeTitulo();
        cout << "Tamanho mínimo não alcançado." << endl;
        cout << "Magnitude definida como 13!" << endl;
        
        magnitude=13;
        
        cin.ignore();
        Menus::ContinuarJogo();
    } else if (magnitude>15){
        Menus::ImprimeTitulo();
        cout << "Tamanho máximo excedido." << endl;
        cout << "Magnitude definida como 15!" << endl;
        
        magnitude=15;
        
        cin.ignore();
        Menus::ContinuarJogo();
    } else {
        cout << "Magnitude definida com sucesso!" << endl;
        cin.ignore();
        Menus::ContinuarJogo();
    }

    // Instanciamento do Jogador Nº1
    // Definição de nome
    Menus::ImprimeTitulo();
    cout << endl << "\t\t\t\t\t JOGADOR Nº1" << endl << endl;
    cout << "Insira seu nome: ";
    cin >> NomeDosPlayers;
    cout << "Nome definido com sucesso!" << endl;
    JogadorN1 = new Player(NomeDosPlayers, magnitude);
    cin.ignore();
    Menus::ContinuarJogo();

    // Escolha de frota 
    Menus::ImprimeTitulo();
    cout << endl << "\t\t\t\t\t JOGADOR Nº1" << endl << endl;
    JogadorN1->informa_frotas();
    cout << endl << "Insira a frota: ";
    cin >> FrotaPlayer;

    // Validação da frota
    if (FrotaPlayer>JogadorN1->get_NumFrotas()){
        FrotaPlayer = JogadorN1->get_NumFrotas();
    
        Menus::ImprimeTitulo();
        cout << endl << "\t\t\t\t\t JOGADOR Nº1" << endl << endl;
        cout << "Limite excedido!" << endl;
        cout << "A frota escolhida será a " << FrotaPlayer << endl;

        cin.ignore();
        Menus::ContinuarJogo();
    } else if (FrotaPlayer<1){
        FrotaPlayer = 1;
            
        Menus::ImprimeTitulo();
        cout << endl << "\t\t\t\t\t JOGADOR Nº1" << endl << endl;
        cout << "Limite excedido!" << endl;
        cout << "A frota escolhida será a " << FrotaPlayer << endl;

        cin.ignore();
        Menus::ContinuarJogo();
    } else {
        cout << "Frota escolhida com sucesso!" << endl;
        cin.ignore();
        Menus::ContinuarJogo();
    }

    // Função para selecionar a frota e montar a matriz        
    JogadorN1->escolhe_frotas(FrotaPlayer);
    Menus::ImprimeTitulo();
    cout << endl << "\t\t\t\t\tFROTA ESCOLHIDA" << endl;
    JogadorN1->imprime_matriz();
    cout << endl << "LEGENDA:" << endl;
    cout << "\t0 = Nenhuma embarcação" << endl;
    cout << "\tC = Canoa" << endl;
    cout << "\tS = Submarino" << endl;
    cout << "\tP = Porta-Aviões" << endl << endl;
    cout << "Esta foi a frota escolhida." << endl;
    Menus::ContinuarJogo();

    // Saída do Jogador Nº1
    Menus::ImprimeTitulo();
    cout << endl << "\t\t\t\t\t JOGADOR Nº1" << endl << endl;
    cout << JogadorN1->get_nome() << " está prontx para jogar!" << endl;
    Menus::ContinuarJogo();

    // Instanciamento do Jogador Nº2
    // Definição de nome
    Menus::ImprimeTitulo();
    cout << endl << "\t\t\t\t\t JOGADOR Nº2" << endl << endl;
    cout << "Insira seu nome: ";
    cin >> NomeDosPlayers;
    cout << "Nome definido com sucesso!" << endl;
    JogadorN2 = new Player(NomeDosPlayers, magnitude);
    cin.ignore();
    Menus::ContinuarJogo();

    // Escolha de frotas
    Menus::ImprimeTitulo();
    cout << endl << "\t\t\t\t\t JOGADOR Nº2" << endl << endl;
    JogadorN2->informa_frotas();
    cout << endl << "Insira a frota: ";
    cin >> FrotaPlayer;

    // Validação da frota escolhida
    if (FrotaPlayer>JogadorN2->get_NumFrotas()){
        FrotaPlayer = JogadorN2->get_NumFrotas();
            
        Menus::ImprimeTitulo();
        cout << endl << "\t\t\t\t\t JOGADOR Nº2" << endl << endl;
        cout << "Limite excedido!" << endl;
        cout << "A frota escolhida será a " << FrotaPlayer << endl;

        cin.ignore();
        Menus::ContinuarJogo();
    } else if (FrotaPlayer<1){
        FrotaPlayer = 1;
            
        Menus::ImprimeTitulo();
        cout << endl << "\t\t\t\t\t JOGADOR Nº2" << endl << endl;
        cout << "Limite excedido!" << endl;
        cout << "A frota escolhida será a " << FrotaPlayer << endl;

        cin.ignore();
        Menus::ContinuarJogo();
    } else {
        cout << "Frota escolhida com sucesso!" << endl;
        cin.ignore();
        Menus::ContinuarJogo();
    }

    // Função para selecionar a frota e montar a matriz
    JogadorN2->escolhe_frotas(FrotaPlayer);         
    Menus::ImprimeTitulo();
    cout << endl << "\t\t\t\t\tFROTA ESCOLHIDA" << endl;
    JogadorN2->imprime_matriz();
    cout << endl << "LEGENDA:" << endl;
    cout << "\t0 = Nenhuma embarcação" << endl;
    cout << "\tC = Canoa" << endl;
    cout << "\tS = Submarino" << endl;
    cout << "\tP = Porta-Aviões" << endl << endl;
    cout << "Esta foi a frota escolhida." << endl;
    Menus::ContinuarJogo();

    // Saída do Jogador Nº2 
    Menus::ImprimeTitulo();
    cout << endl << "\t\t\t\t\t JOGADOR Nº2" << endl << endl;
    cout << JogadorN2->get_nome() << " está prontx para jogar!" << endl;
    Menus::ContinuarJogo();

    // Print na tela para o começo do main loop 
    Menus::ImprimeTitulo();
    cout << endl << "\t\t\t\t\t A BATALHA ESTÁ PRESTES A COMEÇAR!" << endl << endl;
    for (int i=1; i<=magnitude; i++){
        for (int j=1; j<=magnitude; j++){
            JogadorN1->set_MatrizInimiga(i, j, JogadorN2->get_MatrizPlayer(i, j));
            JogadorN2->set_MatrizInimiga(i, j, JogadorN1->get_MatrizPlayer(i, j));
        }
    } 

    // Configurações para o main loop
    MainLoop=true;
    Turno=true;
    Menus::ContinuarJogo();

    // Main loop
    while (MainLoop==true){
        // Turno do JOGADOR N1
        if (Turno==true){   
            Menus::ImprimeTitulo();
            JogadorN1->imprime_MatrizInimiga();
            cout << "Turno de " << JogadorN1->get_nome() << endl;

            // Input de linha
            cout << "Insira uma linha: ";
            cin >> LinhaBarco;
            if (LinhaBarco>magnitude) LinhaBarco=magnitude;
            else if (LinhaBarco<1) LinhaBarco=1;
            
            // Input de coluna
            cout << "Insira uma coluna: ";
            cin >> ColunaBarco;
            if (ColunaBarco>magnitude) ColunaBarco=magnitude;
            else if (ColunaBarco<1) ColunaBarco=1;
            
            Menus::ImprimeTitulo();
            cout << endl;

            // Detecção de colisão e resolução de colisão
            if (JogadorN1->get_MatrizInimiga(LinhaBarco, ColunaBarco) == 0){
                
                cout << "Você não acertou nenhuma embarcação!" << endl;
                JogadorN1->set_MatrizInimiga(LinhaBarco, ColunaBarco, -1);
            
            } else if (JogadorN1->get_MatrizInimiga(LinhaBarco, ColunaBarco) == -1){
                cout << "Você já atirou nesta posição antes! E novamente nada foi atingido." << endl;
            
            // Tiros em canoas
            } else if (JogadorN1->get_MatrizInimiga(LinhaBarco, ColunaBarco) == 1){
                HabilidadeCanoa.habilidade_especial(1);
                cout << " de " << JogadorN2->get_nome() << "!"<< endl;
                JogadorN1->set_MatrizInimiga(LinhaBarco, ColunaBarco, -2);
            
            } else if (JogadorN1->get_MatrizInimiga(LinhaBarco, ColunaBarco) == -2){
                cout << "Você já destruiu esta canoa!" << endl;
            
            // Tiros em submarinos
            } else if (JogadorN1->get_MatrizInimiga(LinhaBarco, ColunaBarco) == 2){
                
                cout << "Você atingiu um submarino de " << JogadorN2->get_nome() << "!" << endl;
                HabilidadeSubmarino.habilidade_especial(1);
                JogadorN1->set_MatrizInimiga(LinhaBarco, ColunaBarco, 4);
            
            } else if (JogadorN1->get_MatrizInimiga(LinhaBarco, ColunaBarco) == 4){

                cout << "Você atingiu um submarino de " << JogadorN2->get_nome() << "!" << endl;
                HabilidadeSubmarino.habilidade_especial(0);
                JogadorN1->set_MatrizInimiga(LinhaBarco, ColunaBarco, -3);

            } else if (JogadorN1->get_MatrizInimiga(LinhaBarco, ColunaBarco) == -3){
                cout << "Você já afundou esta posição de submarino!" << endl;

            // Tiros em porta-aviões
            } else if (JogadorN1->get_MatrizInimiga(LinhaBarco, ColunaBarco) == 3){

                cout << "Você atingiu um porta-aviões de " << JogadorN2->get_nome() << "!" << endl;
                if (HabilidadePortaAvioes.habilidade_especial(ColunaBarco) != 2){
                    JogadorN1->set_MatrizInimiga(LinhaBarco, ColunaBarco, -4);
                }

            } else if (JogadorN1->get_MatrizInimiga(LinhaBarco, ColunaBarco) == -4){
                cout << "Você já afundou esta posição de porta-aviões!" << endl;

            }

    
            cout << endl;
            JogadorN1->imprime_MatrizInimiga();
            cin.ignore();
            Menus::ContinuarJogo();
            if (JogadorN1->verifica_vitoria()==true) goto Vitoria1;
            Turno=false;
        } 
        // Turno do JOGADOR N2
        if (Turno==false){
            Menus::ImprimeTitulo();
            JogadorN2->imprime_MatrizInimiga();
            cout << "Turno de " << JogadorN2->get_nome() << endl;
            
            cout << "Insira uma linha: ";
            cin >> LinhaBarco;
            if (LinhaBarco>magnitude) LinhaBarco=magnitude;
            else if (LinhaBarco<1) LinhaBarco=1;
            
            cout << "Insira uma coluna: ";
            cin >> ColunaBarco;
            if (ColunaBarco>magnitude) ColunaBarco=magnitude;
            else if (ColunaBarco<1) ColunaBarco=1;

            Menus::ImprimeTitulo();
            cout << endl;

            // Tiros na água
            if (JogadorN2->get_MatrizInimiga(LinhaBarco, ColunaBarco) == 0){
                
                cout << "Você não acertou nenhuma embarcação!" << endl;
                JogadorN2->set_MatrizInimiga(LinhaBarco, ColunaBarco, -1);
            
            } else if (JogadorN2->get_MatrizInimiga(LinhaBarco, ColunaBarco) == -1){
                cout << "Você já atirou nesta posição antes! E novamente nada foi atingido." << endl;
            
            // Tiros em canoas
            } else if (JogadorN2->get_MatrizInimiga(LinhaBarco, ColunaBarco) == 1){
                HabilidadeCanoa.habilidade_especial(1);
                cout << " de " << JogadorN1->get_nome() << "!"<< endl;
                JogadorN2->set_MatrizInimiga(LinhaBarco, ColunaBarco, -2);
            
            } else if (JogadorN2->get_MatrizInimiga(LinhaBarco, ColunaBarco) == -2){
                cout << "Você já destruiu esta canoa!" << endl;
            
            // Tiros em submarinos
            } else if (JogadorN2->get_MatrizInimiga(LinhaBarco, ColunaBarco) == 2){
                
                cout << "Você atingiu um submarino de " << JogadorN1->get_nome() << "!" << endl;
                HabilidadeSubmarino.habilidade_especial(1);
                JogadorN2->set_MatrizInimiga(LinhaBarco, ColunaBarco, 4);
            
            } else if (JogadorN2->get_MatrizInimiga(LinhaBarco, ColunaBarco) == 4){

                cout << "Você atingiu um submarino de " << JogadorN1->get_nome() << "!" << endl;
                HabilidadeSubmarino.habilidade_especial(0);
                JogadorN2->set_MatrizInimiga(LinhaBarco, ColunaBarco, -3);

            } else if (JogadorN2->get_MatrizInimiga(LinhaBarco, ColunaBarco) == -3){
                cout << "Você já afundou esta posição de submarino!" << endl;

            // Tiros em porta-aviões
            } else if (JogadorN2->get_MatrizInimiga(LinhaBarco, ColunaBarco) == 3){

                cout << "Você atingiu um porta-aviões de " << JogadorN1->get_nome() << "!" << endl;
                if (HabilidadePortaAvioes.habilidade_especial(ColunaBarco) != 2){
                    JogadorN2->set_MatrizInimiga(LinhaBarco, ColunaBarco, -4);
                }

            } else if (JogadorN2->get_MatrizInimiga(LinhaBarco, ColunaBarco) == -4){
                cout << "Você já afundou esta posição de porta-aviões!" << endl;

            } 

            cout << endl;
            JogadorN2->imprime_MatrizInimiga();
            cin.ignore();
            Menus::ContinuarJogo();
            if (JogadorN2->verifica_vitoria()==true) goto Vitoria2;
            Turno=true;
        }
    }

    Vitoria1:
        Menus::ImprimeTitulo();
        cout << endl << "\t\t\t\t\tVITÓRIA!!" << endl;
        cout << JogadorN1->get_nome() << " derrotou " << JogadorN2->get_nome() << " na Batalha Naval!.";
        cout << endl;
        Menus::RetornaAoMenu();
    Vitoria2:
        Menus::ImprimeTitulo();
        cout << endl << "\t\t\t\t\tVITÓRIA!!" << endl;
        cout << JogadorN2->get_nome() << " derrotou " << JogadorN1->get_nome() << " na Batalha Naval!.";
        cout << endl;
        Menus::RetornaAoMenu();


}

// Void para Criação de frotas
void Menus::CriarFrota(){

    // Limpeza do vector de embarcações
    FrotaBarco.clear();
    
    // Definição da matriz como 0
    for (int i=0; i<16; i++){
        for (int j=0; j<16; j++){
            Matriz[i][j]=0;
        }
    }

    // Parte estética
    Menus::ImprimeTitulo();
    cout << endl << endl << "\t\t\t\t\tCRIAÇÃO DE FROTAS" << endl << endl << endl;
    cout << "Sua frota estará inserida dentro de um mapa de magnitudes:" << endl;
    cout << "\tMínima de 13 dimensões." << endl;
    cout << "\tMáxima de 15 dimensões." << endl << endl;
    cout << "Insira o tamanho do mapa da sua frota: ";
    cin >> magnitude;    

    // Validação de magnitude do mapa
    if (magnitude<13){ 
        Menus::ImprimeTitulo();
        cout << "Tamanho mínimo não alcançado." << endl;
        cout << "Magnitude definida como 13!" << endl;
        
        magnitude=13;
        
        cin.ignore();
        Menus::ContinuarJogo();
    } else if (magnitude>15){
        Menus::ImprimeTitulo();
        cout << "Tamanho máximo excedido." << endl;
        cout << "Magnitude definida como 15!" << endl;
        
        magnitude=15;
        
        cin.ignore();
        Menus::ContinuarJogo();
    }


    Menus::ImprimeTitulo();    
    cout << endl << endl << "\t\t\t\t\tCRIAÇÃO DE FROTAS" << endl << endl << endl;
    cout << "A frota terá, ao todo, 8 embercações." << endl;
    cout << "Embarcações disponíveis:" << endl;
    cout << "\t2 Porta-Aviões" << endl;
    cout << "\t4 Submarinos" << endl;
    cout << "\t8 Canoas" << endl;
    ContinuarJogo();
    
    // Alocando as canoas
    for (NumBarcos=1; NumBarcos<=8; NumBarcos++){
        CriacaoDeCanoa:        
            Menus::ImprimeTitulo();        
            cout << endl << endl << "\t\t\t\t\tCANOAS" << endl << endl << endl;
            cout << NumBarcos <<"ª Canoa" << endl << endl;
            
            // Input para linha
            cout << "Insira a posição da linha: ";
            cin >> LinhaBarco;
            if (LinhaBarco>magnitude) LinhaBarco=magnitude;
            else if (LinhaBarco<0) LinhaBarco=0;

            // Input para coluna
            cout << "Insira a posição da coluna: ";
            cin >> ColunaBarco;      
            if (ColunaBarco>magnitude) ColunaBarco=magnitude;
            else if (ColunaBarco<0) ColunaBarco=0;

            // Detecção de colisão
            if (Matriz[LinhaBarco][ColunaBarco]>0){
                Menus::ImprimeTitulo();
                cout << endl << endl << "\t\t\t\t\tErro!" << endl << endl << endl;
                cout << "Já existe uma embarcação instanciando esta posição!" << endl;
                cout << "Escolha outra posição para a Canoa." << endl;
                
                cin.ignore();
                Menus::ContinuarJogo();
                goto CriacaoDeCanoa;
            } 
            
            // Atualização de embarcações na matriz
            Matriz[LinhaBarco][ColunaBarco]=1;

            // Atualização no vector de embarcações
            FrotaBarco.push_back( new Canoa(LinhaBarco, ColunaBarco));

            cin.ignore();
            ContinuarJogo();
    }

    // Alocando os submarinos
    for (NumBarcos=1; NumBarcos<=4; NumBarcos++){
        CriacaoDeSubmarino:             // Label para uso de recursividade      
            Menus::ImprimeTitulo();        
            cout << endl << endl << "\t\t\t\t\tSUBMARINOS" << endl << endl << endl;
            cout << NumBarcos <<"º Submarino" << endl << endl;
            
            // Input para linha
            cout << "Insira a posição da linha: ";
            cin >> LinhaBarco;
            if (LinhaBarco>magnitude) LinhaBarco=magnitude;
            else if (LinhaBarco<0) LinhaBarco=0;

            // Input para coluna
            cout << "Insira a posição da coluna: ";
            cin >> ColunaBarco;      
            if (ColunaBarco>magnitude) ColunaBarco=magnitude;
            else if (ColunaBarco<0) ColunaBarco=0;

            // Validação de posição
            if (Matriz[LinhaBarco][ColunaBarco]>0){
                Menus::ImprimeTitulo();
                cout << endl << endl << "\t\t\t\t\tErro!" << endl << endl << endl;
                cout << "Já existe uma embarcação instanciando esta posição!" << endl;
                cout << "Escolha outra posição para o Submarino." << endl;
                cin.ignore();
                Menus::ContinuarJogo(); 
                goto CriacaoDeSubmarino;
            }
            
            // Direcionamento do submarino
            DirecionamentoSub:          // Label para uso de recursividade
                Menus::ImprimeTitulo();        
                cout << endl << endl << "\t\t\t\t\tSUBMARINOS" << endl << endl << endl;
                cout << NumBarcos <<"º Submarino" << endl;
                cout << endl << "LEGENDA DE DIRECIONAMENTO:" << endl;
                cout << "\t\"1\" = cima" << endl;
                cout << "\t\"2\" = baixo" << endl;
                cout << "\t\"3\" = direita" << endl;
                cout << "\t\"4\" = esquerda" << endl << endl;
        
                cout << "Insira o direcionamento do submarino: ";
                cin >> DirecionamentoBarco;

                // Validação do input
                if (DirecionamentoBarco.compare("1") && DirecionamentoBarco.compare("2") && DirecionamentoBarco.compare("3") && DirecionamentoBarco.compare("4")){
                    Menus::ImprimeTitulo();
                    cout << endl << endl << "\t\t\t\t\tErro!" << endl << endl << endl;
                    cout << "Direcionamento inválido!" << endl;
                    cout << "Escolha um direcionamento válido." << endl;              
                    cin.ignore();
                    Menus::ContinuarJogo();
                    goto DirecionamentoSub;
                }

                
                // Detecção de colisões com o direcionamento para cima
                if (!DirecionamentoBarco.compare("1")){
                    if (LinhaBarco-1<1){
                        Menus::ImprimeTitulo();
                        cout << endl << endl << "\t\t\t\t\tErro!" << endl << endl << endl;
                        cout << "Limite do mapa excedido!" << endl;
                        cout << "Escolha outro direcionamento para o submarino." << endl;
                        
                        cin.ignore();
                        Menus::ContinuarJogo();
                        goto DirecionamentoSub;
                    } 

                    if (Matriz[LinhaBarco-1][ColunaBarco]>0){
                        Menus::ImprimeTitulo();
                        cout << endl << endl << "\t\t\t\t\tErro!" << endl << endl << endl;
                        cout << "Já existe uma embarcação instanciando a posição:" << endl;
                        cout << "\tLinha:  " << LinhaBarco-1 << endl;
                        cout << "\tColuna: " << ColunaBarco << endl;
                        cout << "Escolha outro direcionamento para o Submarino." << endl;
                        
                        cin.ignore();
                        Menus::ContinuarJogo();
                        goto DirecionamentoSub;
                    }

                    // Atualização de embarcações na matriz e direcionamento
                    for (int i=0; i<2; i++) Matriz[LinhaBarco-i][ColunaBarco]=2;
                    DirecionamentoBarco = "cima";
                }
                // Detecção de colisões com o direconamento para baixo
                if (!DirecionamentoBarco.compare("2")){
                    if (LinhaBarco+1>magnitude){
                        Menus::ImprimeTitulo();
                        cout << endl << endl << "\t\t\t\t\tErro!" << endl << endl << endl;
                        cout << "Limite do mapa excedido!" << endl;
                        cout << "Escolha outro direcionamento para o submarino." << endl;
                        
                        cin.ignore();
                        Menus::ContinuarJogo();
                        goto DirecionamentoSub;
                    } 

                    if (Matriz[LinhaBarco+1][ColunaBarco]>0){
                        Menus::ImprimeTitulo();
                        cout << endl << endl << "\t\t\t\t\tErro!" << endl << endl << endl;
                        cout << "Já existe uma embarcação instanciando a posição:" << endl;
                        cout << "\tLinha:  " << LinhaBarco+1 << endl;
                        cout << "\tColuna: " << ColunaBarco << endl;
                        cout << "Escolha outro direcionamento para o Submarino." << endl;
                        
                        cin.ignore();
                        Menus::ContinuarJogo();
                        goto DirecionamentoSub;
                    }

                    // Atualização de embarcações na matriz e direcionamento
                    for (int i=0; i<2; i++) Matriz[LinhaBarco+i][ColunaBarco]=2;
                    DirecionamentoBarco = "baixo";
                }
                // Detecção de colisões com o direcionamento para direita
                if (!DirecionamentoBarco.compare("3")){
                    if (ColunaBarco+1>magnitude){
                        Menus::ImprimeTitulo();
                        cout << endl << endl << "\t\t\t\t\tErro!" << endl << endl << endl;
                        cout << "Limite do mapa excedido!" << endl;
                        cout << "Escolha outro direcionamento para o submarino." << endl;
                        
                        cin.ignore();
                        Menus::ContinuarJogo();
                        goto DirecionamentoSub;
                    } 

                    if (Matriz[LinhaBarco][ColunaBarco+1]>0){
                        Menus::ImprimeTitulo();
                        cout << endl << endl << "\t\t\t\t\tErro!" << endl << endl << endl;
                        cout << "Já existe uma embarcação instanciando a posição:" << endl;
                        cout << "\tLinha:  " << LinhaBarco << endl;
                        cout << "\tColuna: " << ColunaBarco+1 << endl;
                        cout << "Escolha outro direcionamento para o Submarino." << endl;
                        
                        cin.ignore();
                        Menus::ContinuarJogo();
                        goto DirecionamentoSub;
                    }

                    // Atualização de embarcações na matriz e direcionamento
                    for (int i=0; i<2; i++) Matriz[LinhaBarco][ColunaBarco+i]=2;
                    DirecionamentoBarco = "direita";
                }
                // Detecção de colisões com o direcionamento para esquerda
                if (!DirecionamentoBarco.compare("4")){
                    if (ColunaBarco-1<1){
                        Menus::ImprimeTitulo();
                        cout << endl << endl << "\t\t\t\t\tErro!" << endl << endl << endl;
                        cout << "Limite do mapa excedido!" << endl;
                        cout << "Escolha outro direcionamento para o submarino." << endl;
                        
                        cin.ignore();
                        Menus::ContinuarJogo();
                        goto DirecionamentoSub;
                    } 

                    if (Matriz[LinhaBarco][ColunaBarco-1]>0){
                        Menus::ImprimeTitulo();
                        cout << endl << endl << "\t\t\t\t\tErro!" << endl << endl << endl;
                        cout << "Já existe uma embarcação instanciando a posição:" << endl;
                        cout << "\tLinha:  " << LinhaBarco << endl;
                        cout << "\tColuna: " << ColunaBarco-1 << endl;
                        cout << "Escolha outro direcionamento para o Submarino." << endl;
                        
                        cin.ignore();
                        Menus::ContinuarJogo();
                        goto DirecionamentoSub;
                    }

                    // Atualização de embarcações na matriz e direcionamento
                    for (int i=0; i<2; i++) Matriz[LinhaBarco][ColunaBarco-i]=2;
                    DirecionamentoBarco="esquerda";
                }


            FrotaBarco.push_back( new Submarino(LinhaBarco, ColunaBarco, DirecionamentoBarco));

            cin.ignore();
            ContinuarJogo();
    }    


    // Alocando os porta aviões
    for (NumBarcos=1; NumBarcos<=2; NumBarcos++){
        CriacaoDePortaAvioes:               // Label para uso da recursividade
            Menus::ImprimeTitulo();        
            cout << endl << endl << "\t\t\t\t\tPORTA-AVIÕES" << endl << endl << endl;
            cout << NumBarcos <<"º Porta-Aviões" << endl;
            
            // Input para linha
            cout << "Insira a posição da linha: ";
            cin >> LinhaBarco;
            if (LinhaBarco>magnitude) LinhaBarco=magnitude;
            else if (LinhaBarco<0) LinhaBarco=0;

            // Input para coluna
            cout << "Insira a posição da coluna: ";
            cin >> ColunaBarco;      
            if (ColunaBarco>magnitude) ColunaBarco=magnitude;
            else if (ColunaBarco<0) ColunaBarco=0;

            // Validação de posição
            if (Matriz[LinhaBarco][ColunaBarco]>0){
                Menus::ImprimeTitulo();
                cout << endl << endl << "\t\t\t\t\tErro!" << endl << endl << endl;
                cout << "Já existe uma embarcação instanciando esta posição!" << endl;
                cout << "Escolha outra posição para o Porta-Aviões." << endl;
                cin.ignore();
                Menus::ContinuarJogo(); 
                goto CriacaoDePortaAvioes;
            }

        // Direcionamento do porta-aviões
            DirecionamentoPort:          // Label para uso de recursividade
                
                Menus::ImprimeTitulo();        
                cout << endl << endl << "\t\t\t\t\tPORTA-AVIÕES" << endl << endl << endl;
                cout << NumBarcos <<"º Porta-Aviões" << endl;
                cout << endl << "LEGENDA DE DIRECIONAMENTO:" << endl;
                cout << "\t\"1\" = cima" << endl;
                cout << "\t\"2\" = baixo" << endl;
                cout << "\t\"3\" = direita" << endl;
                cout << "\t\"4\" = esquerda" << endl << endl;
        
                cout << "Insira o direcionamento do porta-aviões: ";
                cin >> DirecionamentoBarco;

                // Validação do input
                if (DirecionamentoBarco.compare("1") && DirecionamentoBarco.compare("2") && DirecionamentoBarco.compare("3") && DirecionamentoBarco.compare("4")){
                    Menus::ImprimeTitulo();
                    cout << endl << endl << "\t\t\t\t\tErro!" << endl << endl << endl;
                    cout << "Direcionamento inválido!" << endl;
                    cout << "Escolha um direcionamento válido." << endl;              
                    cin.ignore();
                    Menus::ContinuarJogo();
                    goto DirecionamentoPort;
                }

                
                // Detecção de colisões com o direcionamento para cima
                if (!DirecionamentoBarco.compare("1")){
                    if (LinhaBarco-3<1){
                        Menus::ImprimeTitulo();
                        cout << endl << endl << "\t\t\t\t\tErro!" << endl << endl << endl;
                        cout << "Limite do mapa excedido!" << endl;
                        cout << "Escolha outro direcionamento para o porta-aviões." << endl;
                        
                        cin.ignore();
                        Menus::ContinuarJogo();
                        goto DirecionamentoPort;
                    } 

                    for (int i=0; i<4; i++){
                        if (Matriz[LinhaBarco-i][ColunaBarco]>0){
                            Menus::ImprimeTitulo();
                            cout << endl << endl << "\t\t\t\t\tErro!" << endl << endl << endl;
                            cout << "Já existe uma embarcação instanciando a posição:" << endl;
                            cout << "\tLinha:  " << LinhaBarco-i << endl;
                            cout << "\tColuna: " << ColunaBarco << endl;
                            cout << "Escolha outro direcionamento para o porta-aviões." << endl;
                            
                            cin.ignore();
                            Menus::ContinuarJogo();
                            goto DirecionamentoPort;
                        }
                    }

                    // Atualização de embarcações na matriz
                    for (int i=0; i<4; i++) Matriz[LinhaBarco-i][ColunaBarco]=3;
                    DirecionamentoBarco="cima";
                }

               
                // Detecção de colisões com o direconamento para baixo
                if (!DirecionamentoBarco.compare("2")){
                    if (LinhaBarco+3>magnitude){
                        Menus::ImprimeTitulo();
                        cout << endl << endl << "\t\t\t\t\tErro!" << endl << endl << endl;
                        cout << "Limite do mapa excedido!" << endl;
                        cout << "Escolha outro direcionamento para o porta-aviões." << endl;
                        
                        cin.ignore();
                        Menus::ContinuarJogo();
                        goto DirecionamentoPort;
                    } 

                    for (int i=0; i<4; i++){
                        if (Matriz[LinhaBarco+i][ColunaBarco]>0){
                            Menus::ImprimeTitulo();
                            cout << endl << endl << "\t\t\t\t\tErro!" << endl << endl << endl;
                            cout << "Já existe uma embarcação instanciando a posição:" << endl;
                            cout << "\tLinha:  " << LinhaBarco+i << endl;
                            cout << "\tColuna: " << ColunaBarco << endl;
                            cout << "Escolha outro direcionamento para o porta-aviões." << endl;
                            
                            cin.ignore();
                            Menus::ContinuarJogo();
                            goto DirecionamentoPort;
                        }
                    }

                    // Atualização de embarcações na matriz
                    for (int i=0; i<4; i++) Matriz[LinhaBarco+i][ColunaBarco]=3;
                    DirecionamentoBarco="baixo";
                }

               
                // Detecção de colisões com o direcionamento para direita
                if (!DirecionamentoBarco.compare("3")){
                    if (ColunaBarco+3>magnitude){
                        Menus::ImprimeTitulo();
                        cout << endl << endl << "\t\t\t\t\tErro!" << endl << endl << endl;
                        cout << "Limite do mapa excedido!" << endl;
                        cout << "Escolha outro direcionamento para o porta-aviões." << endl;
                        
                        cin.ignore();
                        Menus::ContinuarJogo();
                        goto DirecionamentoPort;
                    } 

                    for (int i=0; i<4; i++){
                        if (Matriz[LinhaBarco][ColunaBarco+i]>0){
                            Menus::ImprimeTitulo();
                            cout << endl << endl << "\t\t\t\t\tErro!" << endl << endl << endl;
                            cout << "Já existe uma embarcação instanciando a posição:" << endl;
                            cout << "\tLinha:  " << LinhaBarco << endl;
                            cout << "\tColuna: " << ColunaBarco+i << endl;
                            cout << "Escolha outro direcionamento para o porta-aviões." << endl;
                            
                            cin.ignore();
                            Menus::ContinuarJogo();
                            goto DirecionamentoPort;
                        }
                    }

                    // Atualização de embarcações na matriz
                    for (int i=0; i<4; i++) Matriz[LinhaBarco][ColunaBarco+i]=3;
                    DirecionamentoBarco="direita";
                }

              
                // Detecção de colisões com o direcionamento para esquerda
                if (!DirecionamentoBarco.compare("4")){
                    if (ColunaBarco-3<1){
                        Menus::ImprimeTitulo();
                        cout << endl << endl << "\t\t\t\t\tErro!" << endl << endl << endl;
                        cout << "Limite do mapa excedido!" << endl;
                        cout << "Escolha outro direcionamento para o porta-aviões." << endl;
                        
                        cin.ignore();
                        Menus::ContinuarJogo();
                        goto DirecionamentoPort;
                    } 

                    for (int i=0; i<4; i++){
                        if (Matriz[LinhaBarco][ColunaBarco-i]>0){
                            Menus::ImprimeTitulo();
                            cout << endl << endl << "\t\t\t\t\tErro!" << endl << endl << endl;
                            cout << "Já existe uma embarcação instanciando a posição:" << endl;
                            cout << "\tLinha:  " << LinhaBarco << endl;
                            cout << "\tColuna: " << ColunaBarco-i << endl;
                            cout << "Escolha outro direcionamento para o porta-aviões." << endl;
                            
                            cin.ignore();
                            Menus::ContinuarJogo();
                            goto DirecionamentoPort;
                        }
                    }

                    // Atualização de embarcações na matriz
                    for (int i=0; i<4; i++) Matriz[LinhaBarco][ColunaBarco-i]=3;
                    DirecionamentoBarco="esquerda";
                }

        
        FrotaBarco.push_back( new PortaAvioes(LinhaBarco, ColunaBarco, DirecionamentoBarco));

        cin.ignore();
        ContinuarJogo();

    }

    GuardaFrota = new Mapa(magnitude);
    GuardaFrota->adiciona_frota(FrotaBarco);
    GuardaFrota->armazena_mapa();
    delete GuardaFrota;

    FrotaBarco.clear();
    Menus::ImprimeTitulo();
    cout << "Frota criada com sucesso!" << endl;
    Menus::RetornaAoMenu();
}


// Void para imprimir dados sobre o jogo
void Menus::Sobre_O_Jogo(){
    Menus::ImprimeTitulo();
    cout << "\"BATALHA NAVAL\" um jogo desenvolvido para a disciplina de Orientação a Objetos da Universidade de Brasília - Faculdade do Gama (UnB-FGA)" << endl;
    cout << "\t*O jogo usa como base as regras do clássico jogo de tabuleiro \"Batalha Naval\"" << endl;
    cout << endl << endl;
    cin.ignore();
    Menus::RetornaAoMenu();
}


// Função para sair do jogo
int Menus::SaidaDoJogo(){
    // Label para utilização de recursividade
    MenuDaSaida:

        // Validação da saída
        Menus::ImprimeTitulo();
        cout << endl << endl << "Você tem certeza que deseja sair do jogo?" << endl;
        cout << "1) Sim" << endl;
        cout << "2) Não" << endl << endl;
        Menus::EscolhaOpcao();

        // Validação do Input do usuário
        while (ControleDeOpcao.compare("1") && ControleDeOpcao.compare("2")){
            goto MenuDaSaida;
        }

        // Lógica para saída do jogo
        if (!ControleDeOpcao.compare("1")){
            Menus::ImprimeTitulo();
            cout << endl << "Até mais!" << endl << endl;
            return 0;
        } else Menus::MenuPrincipal();
    
    return 0;
}


// Função para as opções do jogo
int Menus::MenuPrincipal(){
    // Label para utilização de recursividade
    MenuDoJogo:

        // Parte estética do menu
        Menus::ImprimeTitulo();
        cout << endl << endl;
        Menus::ImprimeMenuOpcoes();
        cout << endl;
        
        // Controle de escolhas
        cout << "1) Multiplayer" << endl;
        cout << "2) Criar frota" << endl;
        cout << "3) Sobre o jogo" << endl;
        cout << "4) Sair do jogo" << endl << endl;
        Menus::EscolhaOpcao();

        // Validação de escolha
        while (ControleDeOpcao.compare("1") && ControleDeOpcao.compare("2") && ControleDeOpcao.compare("3") && ControleDeOpcao.compare("4") ){
            goto MenuDoJogo;
        } 

        /* Primeira opção */
        if (!ControleDeOpcao.compare("1")){
            Menus::Multiplayer();

        /* Segunda opção */
        } else if (!ControleDeOpcao.compare("2")){
            Menus::CriarFrota();

        /* Terceira opção */
        } else if (!ControleDeOpcao.compare("3")){
            Menus::Sobre_O_Jogo();
        
        /* Quarta opção */
        } else if (!ControleDeOpcao.compare("4")){
            Menus::SaidaDoJogo();  
        } 

    return 0;
}